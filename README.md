**pleb-bot** is a Discord.JS bot. Currently allows for playing sounds and adding sounds to a list.

# Installation
pleb-bot uses Redis for data storage so you have to install a Redis server.

Afterwards, run: `npm install`

You need to have your Discord Bot Token saved as a environment variable with the name DISCORD\_BOT\_TOKEN
